# Event Tracker App Building Process
----
### Creating and initializing app :
  - navigate to [firebase_console]
  - Create a new project
  - In console -> overview -> click Add Firebase to your WebApp
  - copy the code snippet and place the copied code into the project/app/app.module.ts file after the import statements
- configure the Auth Providers and initializeApp
- Add Users required for mainting the Firebase console
- Add Billing Account for the App

Run this command in project root folder and in project-folder/resources/firebase/functions
```sh
$ npm install
```
delete .firebaserc in project-folder/resources/firebase folder

run thesse command in project-folder folder
````
$cd resources/firebase
$ firebase init
````
Select firebase functions and database and then select the project name from the cli

### Setting Database Rules
The database.rules.json
Specify the rules and add ACL for the database in the project-folder/resources/firebase

This commands sets the database rules for the app:
```sh
$ firebase deploy --only database
```
### Setting App name and Bundle Id
- change the name of the widget id in config.xml file which is in project-root folder
- change the description and other details
### Add Android/IOS app
- Goto Annalytics from Firebase Console and click on android icon
- enter the package name like "com.inqude.eventtracker.eventname"
- download the google-services.json/Google-Services.plist file and place it in root folder
- then build the apk

### To add a client app & enable authentication  for Firebase:

  - Goto Authentication from firebase console Enable Required Providers
  - In Authorized Domains Section Clik on Add Domain
  - enter the domain address of the Admin hosted app
  - click on ok

### To enable Cloud Functions deployment:
- Navigate to Project Settings from the console
- Click on Service Account -> Firebase Admin Sdk -> In Admin SDK configuration snippet
- Choose NodeJS -> Click on  'Generate new private' key button
 - rename the Downloaded file to 'serviceAccountKey'
 - place the file in project/resources/firebase/functions
 - then change the databaseurl variable to the new app's database url in project-folder/resources/firebase/functions/index.js file
 .
 Run below command to deploy cloud functions
````
$ firebase deploy --only functions
````
After  deploying
change the firebase url in the project-folder/src/services/services.ts file  to new urls
example:
'https://us-central1-fir-eventtracker.cloudfunctions.net/createCustomToken'
to
'https://us-central1-appName.cloudfunctions.net/createCustomToken'

###   Generating the App specific Icons and Splash Screen:
- Place the image for Icon named icon in the project-folder/resources folder, must be .png/.ai/.psd format
- Edit the splash  file using photoshop to create a new file, must be .png/.ai/.psd format

run the command to genearate icons and splashscreen:
```sh
$ ionic resources
```
Steps to genrate the favicon for desktop version
- navigate to [favicon_genrator]
- upload the png image and clik on faviconit button
- download the favicons file and unzip it
- place the files of unzipped folder in project-folder/src/assets/icon
- open the faviconit-instructions.txt file and copy the code
- place it in the project/src/index.html in head tag

### To enable cors policy for firebase storage
login to gsutil from commandline
```sh
$ cd ~/project-folder
$ gsutil cors set cors.json <storage bucket url>
```

### Todos
```
$ ionic platform addd android
ionic build android
```
```
$ ionic platform addd ios
ionic build ios
```
 - Setup a heroku client for desktop version
 - Setup code-ship for the build-scripts

License
----

Inqude Solutions Private Limited

   [firebase_console]: <https://console.firebase.google.com>
   [favicon_genrator]: <http://faviconit.com>
