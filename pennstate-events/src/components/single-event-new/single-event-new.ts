import { Component, Input } from "@angular/core";
import * as moment from "moment-timezone";
import { ModalController, NavController } from "ionic-angular";
import { EventDetailPage } from "../../pages/event-detail/event-detail";
import { EventTicket } from "../event-ticket/event-ticket";
import { Talisma } from "../../providers/crm/talisma";
import { UserData } from "../../providers/user-data";

@Component({
  selector: "single-event-new",
  templateUrl: "single-event-new.html"
})
export class SingleEventNew {
  @Input("params") event: any;
  moment = moment;
  participationStatus: any;
  uid: string;

  constructor(public modalCtrl: ModalController, private navCtrl: NavController, private talisma: Talisma, private userData: UserData) {}

  ngOnChanges() {
    this.userData
      .getUid()
      .then((uid) => {
        this.uid = uid;
        return this.talisma.participants.getFirebaseParticipationStatus(this.event.EventId, this.uid);
      })
      .then((result: any) => {
        if (result && result.$exists()) {
          this.participationStatus = result.status;
        }
      });
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }

  viewTicket(event: any, eventData: any) {
    event.stopPropagation();
    if (this.participationStatus == "Registered") {
      let modal = this.modalCtrl.create(EventTicket, { eventId: eventData.EventId }, { enableBackdropDismiss: true });
      modal.present();
    }
  }
}
