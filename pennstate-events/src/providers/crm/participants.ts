import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';
import { HelperService } from '../helperService';
import { AngularFireOfflineDatabase } from 'angularfire2-offline';

@Injectable()
export class Participants extends BaseClass {

  constructor(
    public apiService: ApiService,
    public helperService: HelperService,
    public afoDatabase: AngularFireOfflineDatabase
  ) {
    super(apiService);
    this.key = 'ParticipantId'
    this.url = 'Participants'
  }

  getParticipationStatus(eventId: any, participantId: any) {
    return super.find("EventNameId eq " + eventId + " and ContactNameId eq " + participantId)
  }

  getFirebaseParticipationStatus(eventId: any, uid: any) {
    return new Promise((resolve) => {
      this.afoDatabase.object('/participants/' + eventId + '/' + uid)
        .take(1)
        .subscribe((data) => {
          resolve(data);
        })
    })
  }

  createParticipant(eventId: any, contactId: any) {
    return new Promise((resolve) => {
      let subURl = 'CreateParticipant?eventid=' + eventId + '&contactid=' + contactId;
      let url = this.apiService.getUrl(subURl, 'participant');
      this.apiService.ajax(url, 'string')
        .then((response: any) => {
          resolve(response._body);
        })
        .catch((err: any) => {
          resolve();
        })
    });
  }

  updateParticipant(eventid: any, participantId: any, status: any) {
    return new Promise((resolve, reject) => {
      let subURl = 'UpdateParticipant?eventid=' + eventid + '&participantid=' + participantId + '&participantstatus=' + status;
      let url = this.apiService.getUrl(subURl, 'participant');
      this.apiService.ajax(url, 'string')
        .then((response: any) => {
          resolve(response._body);
        })
        .catch((err: any) => {
          resolve();
        });
    });
  }

  // get event data associated with respective contactId
  getParticipantEvents(contactId: any, fields: any) {
    return new Promise((resolve) => {
      let query = '?$select=EventNameId,NavigationEventNameId&$filter=ContactNameId eq ' + contactId + '&$expand=NavigationEventNameId($select=' + fields + ')';
      let url = this.url + query;
      resolve(this.apiService.ajax(this.apiService.getUrl(url), 'object'));
    });
  }

}
