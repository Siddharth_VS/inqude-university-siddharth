import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { FirebaseAnalyticsProvider } from '../providers/firebase-analytics-provider';
@Directive({
  selector: '[fireTrack]',
  providers: [FirebaseAnalyticsProvider]
})
export class FireTrackDirective {
  @Input('fireTrack') track: string;
  @Input('fireTrackData') data: string;

  constructor(
    private el: ElementRef,
    public fireAnalytics: FirebaseAnalyticsProvider
  ) { }
  @Output() myClick: EventEmitter<any> = new EventEmitter();
  @HostListener('click', ['$event'])
  onClick(e: any) {
    if (this.track && this.data) {
      this.fireAnalytics.checkPlatform().then(() => {
        this.fireAnalytics.logEvent(this.track, this.data);
      })
    }
  }
}
