import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
import { HelperService } from '../../../providers/helperService';
import * as _ from 'underscore';

@Component({
  selector: 'page-add-user',
  templateUrl: 'add-user.html'
})

export class AddUser {
  users: any = [];
  userIds: any[] = [];
  uid: any;
  preSelectedUsers: any = [];
  selectedUsers: any = [];
  query: string = '';
  filterKeys: any = ['firstName', 'lastName', 'email'];

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public afoDatabase: AngularFireOfflineDatabase,
    public helperService: HelperService
  ) {
    this.helperService.getUid()
      .then((value: any) => {
        this.uid = value;
        this.preSelectedUsers = this.navParams.get('users');

        afoDatabase.list('/users', { query: { orderByChild: 'firstName' } })
          .subscribe((snapshots: any) => {
            let temp: any[] = [], pos: any;
            for (let snapshot of snapshots) {
              if (snapshot.$key && snapshot.$key.length && snapshot.status) {
                if (this.isPreSelectedUser(snapshot.$key)) {
                  snapshot.isSelected = true;
                } else {
                  snapshot.isSelected = false;
                }
                pos = (_.findIndex(temp, { '$key': snapshot.$key }))
                if (pos == -1 && snapshot.uid !== this.uid) {
                  temp.push(snapshot);
                }
              }
            }
            this.users = Object.assign([], _.sortBy(temp, 'firstName'));
          });
      });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  addModerators() {
    this.viewCtrl.dismiss(this.userIds);
  }

  isPreSelectedUser(userId: any) {

    let pos: any;
    pos = this.selectedUsers.indexOf(userId);
    if (pos > -1) {
      return true;
    } else {
      return false;
    }
  }

  getClass(item: any) {
    return ((this.preSelectedUsers.indexOf(item.uid)) > -1) ? 'selected-user' : '';
  }

  chooseUsers(user: any) {
    if (this.preSelectedUsers.indexOf(user.uid) < 0) {
      let self = this;
      let isPresent = self.userIds.indexOf("" + user.uid);
      if (isPresent >= 0) {
        user.isSelected = false;
        self.userIds.splice(isPresent, 1);
      } else {
        user.isSelected = true;
        self.userIds.push("" + user.uid);
      }
    }
  }

}
