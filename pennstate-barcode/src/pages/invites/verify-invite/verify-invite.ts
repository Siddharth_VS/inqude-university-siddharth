import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HelperService } from '../../../providers/helperService';
import { UserData } from '../../../providers/user-data';
import { UserDetailFormPage } from '../../user-account/edit-user/edit-user';
import * as _ from 'underscore';

@Component({
  selector: 'page-verify-invite',
  templateUrl: 'verify-invite.html'
})

export class VerifyInvite {
  user: any = {};

  constructor(
    public navCtrl: NavController,
    public helperService: HelperService,
    public userData: UserData
  ) {
  }

  onSubmit(){
    this.helperService.showLoading();
    this.helperService.isUserExists(this.user.email)
    .then((user:any) => {
      let index = _.findIndex(user, {'status': 'accepted'});
      if(index > -1){
        this.helperService.showMessage('Already an member, login to continue');
        this.helperService.hideLoading();
        return false;
      }
      this.helperService.hideLoading();
      if(user && user[0] && user[0].isUserExists && this.user.code == user[0].code) {
        this.helperService.showMessage('User validated successfully');
        this.navCtrl.setRoot(UserDetailFormPage, { user: user[0] ,isNewUser:true});
      } else {
        this.helperService.showMessage('Enter valid credentials');
      }
    });
  }
}
