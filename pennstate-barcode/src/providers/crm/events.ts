import { Injectable } from '@angular/core';
import { ApiService } from './apiService';
import { BaseClass } from './base';

@Injectable()
export class Events extends BaseClass {
  constructor(
    public apiService: ApiService
  ) {
    super(apiService);
    this.key = 'EventId';
    this.url = 'Events'
  }

  getEvents(number: number, startPoint?: number, filter?: string) {
    let subURl = this.url + '?' + (filter ? (filter + '&') : '') + '$skip=' + (startPoint ? startPoint : 0) + '&$top=' + number;
    return this.apiService.ajax(this.apiService.getUrl(subURl), 'object')
  }

  getModeratorEvents(fields: string, filter?: string) {
    let subURl = this.url + '?' + (filter ? (filter + '&') : '') + '&$select=' + fields;
    return this.apiService.ajax(this.apiService.getUrl(subURl), 'object')
  }

  getEventById(eventId: number) {
    return super.find("EventId eq " + eventId);
  }
}
