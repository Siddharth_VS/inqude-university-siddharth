import { Component, Input } from '@angular/core';
import * as moment from 'moment-timezone';
import { NavController } from 'ionic-angular';
import { EventDetailPage } from '../../pages/event-detail/event-detail';

@Component({
  selector: 'single-event',
  templateUrl: 'single-event.html'
})

export class SingleEvent {
  @Input('params') event: any;
  timeZone: string = 'America/Matamoros';

  constructor(
    private navCtrl: NavController
  ) { }

  getClass(event: any) {
    return ((moment(event.EventStartDate).tz(this.timeZone).format('x')) >= (moment().tz(this.timeZone).subtract(1, 'days').endOf('day').format('x'))) ? '' : 'past-event';
  }

  viewEvent(eventData: any) {
    this.navCtrl.push(EventDetailPage, { event: eventData });
  }

}
