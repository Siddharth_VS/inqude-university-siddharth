import { Component, Input } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { UserData } from '../../providers/user-data';
import { AngularFireOfflineDatabase } from 'angularfire2-offline/database';
@Component({
  selector: 'like',
  templateUrl: 'like.html',
  providers: [UserData]
})

export class Like {
  @Input('params') params: any;
  @Input('data') data: any;
  uid: string;
  isLiked: boolean;
  constructor(
    public af: AngularFire,
    public afoDatabase: AngularFireOfflineDatabase,
    public userData: UserData
  ) {
    this.userData.getUid().then(_uid => {
      if (_uid) {
        this.uid = _uid;
      }
      this.liked();
      this.getLikesCount();
    });
  }
  like() {
    if (!!!this.params.disableLike && this.uid) {
      this.isLiked = !this.isLiked;
      if (this.isLiked === false) {
        this.af.database.object('likes/' + this.data.type + '/' + this.data.id + '/users/' + this.uid).remove();
      } else {
        this.af.database.object('likes/' + this.data.type + '/' + this.data.id + '/users/' + this.uid)
          .set(this.isLiked);
      }
    }
  }

  liked() {
    let self: any = this;
    this.af.database.object('likes/' + this.data.type + '/' + this.data.id + '/users/' + this.uid)
      .take(1).subscribe((res) => {
        self.isLiked = (res.$value === null) ? false : res.$value;
      })
  }
  getLikesCount() {
    let self: any = this;
    this.afoDatabase.object('likes/' + this.data.type + '/' + this.data.id, { preserveSnapshot: true })
      .subscribe((res) => {
        if (res) {
          self.likes = res.count;
        }
      })
  }

}
